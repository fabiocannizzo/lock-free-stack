#include "LockFreeIndexStack.h"

using namespace LockFree;

#include <thread>
#include <iostream>
#include <cassert>
#include <algorithm>

const size_t stack_size = 8;
const size_t n_threads = 10;

typedef LockFreeIndexStack lf_stack_t;

static void foo(std::atomic<bool>* go, lf_stack_t *stack)
{
    while (!*go)
        std::this_thread::yield();
    for (size_t i = 0; i < 1000000; ++i) {
        lf_stack_t::index_t index;
        while (!(index = stack->pop()).isValid())
            std::this_thread::yield();
        std::this_thread::yield();
        stack->push(index);
        std::this_thread::yield();
    }
}

static void test()
{
    lf_stack_t stack((lf_stack_t::index_t) stack_size);

    std::atomic<bool> go(false);
    std::vector<std::unique_ptr<std::thread> > threads(n_threads);
    for (auto& t : threads)
        t.reset(new std::thread(foo, &go, &stack));
    go = true;
    for (const auto& t : threads)
        t->join();

    assert(stack.is_lock_free());\

#ifdef LOCK_FREE_STACK_DEBUG
    std::cout << "n ops " << stack.counter() << "\n";
    std::cout << "n pop fail " << stack.pop_fail() << "\n";
    std::cout << "n pop spin " << stack.pop_spin() << "\n";
    std::cout << "n push spin " << stack.push_spin() << "\n";
#endif

    std::cout << "final stack state: ";
    lf_stack_t::index_t next;
    stack.final_assert();
    for(size_t i = 0; i < stack.capacity(); ++i) {
        next = stack.pop();
        std::cout << next << ", ";
    }
    std::cout << "\n";

    std::cout << "test success\n";
}
