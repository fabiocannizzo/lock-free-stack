CC = g++
CFLAGS := -O2 -std=c++11

all:
	$(CC) $(CFLAGS) -Wl,-lpthread test.cpp test1.cpp test2.cpp -o test.exe

.PHONY: clean
clean:
	rm *.exe 2>/dev/null
