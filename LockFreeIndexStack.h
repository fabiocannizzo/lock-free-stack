#pragma once

#include <cstdint>
#include <vector>
#include <atomic>
#include <cassert>

namespace LockFree
{

    /*
      A simple lock free index stack taken from:
      https://bitbucket.org/fabiocannizzo/lock-free-stack

      An alternative implementation is in std::lock_free, but I could not get it to work with VS2008.

      The stack class has a fixed size, which must be stated when the class is initially constructed.
      Once created, the stack is full of 32 bits unsigned integers in the range 0..size-1.

      Usage example:

        LockFreeIndexStack stack(4);  // creates a stack containinig: 0, 1, 2, 3
        LockFreeIndexStack::index_t index = stack.pop(); // Get an index from the stack. Spin locks until one is available.
        // ... do something with 'index'
        stack.push(index);   // return index to the stack

    */

    class LockFreeIndexStack
    {
        typedef uint32_t aux_index_t;
        typedef uint64_t bundle_t;
    public:
        static const aux_index_t s_null = ~aux_index_t(0);

        struct index_t
        {
            typedef aux_index_t type;
            index_t(type index = s_null) : m_index(index) {}
            bool isValid() const { return m_index != s_null; }
            operator type&() { return m_index; }
            operator type() const { return m_index; }
            type m_index;
        };

    private:
        struct alignas(alignof(bundle_t)) Bundle
        {
            Bundle(index_t index, index_t count)
                : m_index(index)
                , m_count(count)
            {
            }

            aux_index_t m_index;
            aux_index_t m_count;  // this is to probabilistically avoid the ABA problem (Treiber)
        };

        typedef std::atomic<Bundle> atomic_bundle_t;
        typedef std::atomic<uint64_t> atomic_uint64_t;

    public:
        LockFreeIndexStack(index_t n)
            : m_top(Bundle(0, 0))
            , m_next(n, s_null)
#ifdef LOCK_FREE_STACK_DEBUG
            , m_pop_spin(0)
            , m_pop_fail(0)
            , m_push_spin(0)
#endif
        {
            // init stack entries
            for (index_t i = 1; i < n; ++i)
                m_next[i - 1] = i;
        }

        index_t top() const
        {
            return Bundle(m_top).m_index;
        }

        index_t counter() const
        {
            return Bundle(m_top).m_count;
        }

#ifdef LOCK_FREE_STACK_DEBUG
        uint64_t pop_spin() const
        {
            return m_pop_spin;
        }
        uint64_t push_spin() const
        {
            return m_push_spin;
        }
        uint64_t pop_fail() const
        {
            return m_pop_fail;
        }
#endif

        // non thread safe
        void final_assert()
        {
            const size_t n = m_next.size();
            index_t next = Bundle(m_top).m_index;
            std::vector<int> check(n, 0);
            size_t nIter = 0;
            while(next != s_null && nIter++ < n) {
                assert(next < n);
                ++check[next];
                next = m_next[next];
            }
            for (size_t i = 0; i < n; ++i)
                assert(check[i] == 1);
        }


        // lock free pop
        // if the stack is empty returns an invalid index
        index_t pop()
        {
            Bundle curtop(m_top.load());
            while(true) {
                index_t candidate = curtop.m_index;

                if (candidate != s_null) {
                    index_t next = m_next[candidate];
                    Bundle newtop(next, curtop.m_count);
                    // In the very remote eventuality that, between reading 'm_top' and
                    // the next line other threads cause all the below circumstances occur simultaneously:
                    // - other threads execute exactly a multiple of 2^32 push operations,
                    //   so that 'm_count' assumes again the original value;
                    // - the value read as 'candidate' 2^32 transactions ago is again top of the stack;
                    // - the value 'm_next[candidate]' is no longer what it was 2^32 transactions ago
                    // then the stack will get corrupted
                    if (m_top.compare_exchange_weak(curtop, newtop)) {
                        return candidate;
                    }
                    else {
    #ifdef LOCK_FREE_STACK_DEBUG
                        ++m_pop_spin;
    #endif
                    }
                }
                else {
                    // stack is empty
    #ifdef LOCK_FREE_STACK_DEBUG
                    ++m_pop_fail;
    #endif
                    return s_null;
                }
            }
        }

        index_t capacity() const { return (index_t) m_next.size(); }

        static bool is_lock_free() { return atomic_bundle_t(Bundle(0, 0)).is_lock_free(); }

        static bool is_valid(index_t index) { return index.isValid(); }

        // lock free push
        // indices pushed must:
        // - belong to the range of the stack
        // - not be currently contained in the stack
        // If any of the two conditions is violated the stack will get corrupted
        void push(index_t index)
        {
            Bundle curtop(m_top.load());
            while (true) {
                index_t current = curtop.m_index;
                m_next[index] = current;
                Bundle newtop = Bundle(index, curtop.m_count + 1);
                if (m_top.compare_exchange_weak(curtop, newtop)) {
                    return;
                }
                else {
#ifdef LOCK_FREE_STACK_DEBUG
                    ++m_push_spin;
#endif
                }
            };
        }

    private:
        atomic_bundle_t m_top;
        std::vector<index_t> m_next;

#ifdef LOCK_FREE_STACK_DEBUG
        atomic_uint64_t m_pop_spin;
        atomic_uint64_t m_pop_fail;
        atomic_uint64_t m_push_spin;
#endif
    };

} // namespace LockFree
